package io.shodo.fizzbuzz;

import org.junit.jupiter.api.Assertions;

class FizzbuzzTestHelper {
  FizzbuzzTestHelper(int value) {
    this.result = new Fizzbuzz().evaluate(value);
  }

  String result;

  void shouldBe(String expected) {
    Assertions.assertEquals(expected, result);
  }
}
