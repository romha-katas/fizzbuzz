package io.shodo.fizzbuzz;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FizzbuzzTest {

  @Test
  @DisplayName("Should display Fizz if multiple of 3")
  void test1() {
    fizzbuzz_of(3).shouldBe("Fizz");
    fizzbuzz_of(3 * 2).shouldBe("Fizz");
  }

  @Test
  @DisplayName("Should display Buzz if multiple of 5")
  void test2() {
    fizzbuzz_of(5).shouldBe("Buzz");
    fizzbuzz_of(5 * 2).shouldBe("Buzz");
  }

  @Test
  @DisplayName("Should display the number otherwise")
  void test3() {
    fizzbuzz_of(4).shouldBe("4");
  }

  @Test
  @DisplayName("Should display FizzBuzz if multiple of 3 and 5")
  void test4() {
    fizzbuzz_of(3 * 5).shouldBe("FizzBuzz");
  }

  @Test
  @DisplayName("0 is not a multiple of 3, neither of 5")
  void test5() {
    fizzbuzz_of(0).shouldBe("0");
  }

  private FizzbuzzTestHelper fizzbuzz_of(int value) {
    return new FizzbuzzTestHelper(value);
  }

}
